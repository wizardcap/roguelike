from bearlibterminal import terminal as blt

from Classes.Object import Object
from Classes.Tile import Tile

SCREEN_WIDTH = 80
SCREEN_HEIGHT = 50
LIMIT_FPS = 20
GAME_NAME = "ROGUE TUTORIAL"

MAP_WIDTH = 80
MAP_HEIGHT = 45

def main():
    player = Object(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, '@', 'White')
    npc = Object(1, 1, 'X', 'White')
    make_map()
    game_map[2][2].blocked = False
    game_map[2][2].block_sight = False
    objects = [npc, player]

    blt.open()
    blt.set("window: title='{}', size={}x{}".format(GAME_NAME, SCREEN_WIDTH, SCREEN_HEIGHT))

    while True:
        blt.clear()
        
        render_all(objects)
            
        blt.refresh()
        key = blt.read()

        if key in (blt.TK_ESCAPE, blt.TK_CLOSE):
            break
        else:
            handle_keys(key, player)
            
    blt.close()

def handle_keys(key, player):
    # movement keys
    if key == blt.TK_UP:
        player.y -= 1
    elif key == blt.TK_DOWN:
        player.y += 1
    elif key == blt.TK_LEFT:
        player.x -= 1
    elif key == blt.TK_RIGHT:
        player.x += 1

def render_all(objects):

    for y in range(MAP_HEIGHT):
        for x in range(MAP_WIDTH):
            wall = game_map[x][y].block_sight
            if wall:
                blt.printf(x, y, ' ')
            else:
                blt.printf(x, y, '#')
                
    for object in objects:
        object.draw()

def make_map():
    global game_map

    #fill game_map with unblocked tiles
    game_map = [[Tile(True)
            for y in range(MAP_HEIGHT)]
           for x in range(MAP_WIDTH)]




if __name__ == '__main__':
    main()