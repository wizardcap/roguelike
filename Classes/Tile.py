from bearlibterminal import terminal as blt
class Tile:
    def __init__(self, blocked, block_sight = None):
        self.blocked = blocked
        
        block_sight = blocked if block_sight is None else None
        self.block_sight = block_sight